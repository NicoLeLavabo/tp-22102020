-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 25 oct. 2020 à 22:34
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `ID` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`ID`, `nom`) VALUES
(1, 'Divers'),
(2, 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `ID` int(11) UNSIGNED NOT NULL,
  `message_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_reponse` text NOT NULL,
  `message_admin` tinyint(1) NOT NULL DEFAULT '0',
  `ID_ticket` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reponse`
--

INSERT INTO `reponse` (`ID`, `message_date`, `message_reponse`, `message_admin`, `ID_ticket`) VALUES
(6, '2020-10-22 22:10:01', 'oui allo ???', 0, 2),
(7, '2020-10-22 22:28:19', 'Bonjour allez voir ici aaaaaaaa', 0, 2),
(8, '2020-10-22 22:33:01', 'dzqdsqdzsq', 0, 2),
(9, '2020-10-22 22:36:49', 'oui allo ?', 0, 3),
(10, '2020-10-22 22:56:06', 'fzqfzqfzqfs', 1, 2),
(11, '2020-10-22 23:26:26', 'fzqfsfsqfqz', 0, 2),
(12, '2020-10-22 23:26:29', 'fzqsfzqs', 0, 2),
(13, '2020-10-22 23:27:01', 'dzqdsdzqsdzq', 1, 2),
(14, '2020-10-23 14:03:21', 'fzqsfzqsfdzq', 1, 2),
(15, '2020-10-23 14:04:09', 'fzqsfzq', 1, 2),
(16, '2020-10-23 14:11:27', 'ok merci', 0, 2),
(17, '2020-10-23 14:12:57', 'zfqsfzq', 1, 2),
(18, '2020-10-24 16:05:05', 'Test message admin', 1, 3),
(20, '2020-10-24 17:06:42', 'ok ca marche', 0, 2),
(21, '2020-10-25 18:51:01', 'Test message admin update auto statut', 1, 6),
(22, '2020-10-25 18:56:10', 'Bonjour que puis-je pour vous', 1, 5),
(23, '2020-10-25 22:20:39', 'Aucune reponse ?', 0, 9);

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE `ticket` (
  `ID` int(11) UNSIGNED NOT NULL,
  `statut` varchar(255) NOT NULL DEFAULT 'ouvert',
  `date_ticket` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_ticket` text NOT NULL,
  `numAlea` int(11) NOT NULL,
  `isvisible` tinyint(1) NOT NULL DEFAULT '0',
  `ID_user` int(11) NOT NULL,
  `ID_categorie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`ID`, `statut`, `date_ticket`, `message_ticket`, `numAlea`, `isvisible`, `ID_user`, `ID_categorie`) VALUES
(2, 'resolu', '2020-10-22 22:09:46', 'j\'ai un soucis ', 64418029, 1, 4, 2),
(3, 'resolu', '2020-10-22 22:11:02', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 26114196, 1, 4, 1),
(4, 'ouvert', '2020-10-23 15:05:26', 'je suis un ticket test', 43653433, 0, 4, 2),
(5, 'en cours', '2020-10-23 16:18:51', 'bonjour', 18698425, 0, 5, 1),
(6, 'en cours', '2020-10-24 14:23:09', 'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).', 43967483, 0, 5, 1),
(7, 'ouvert', '2020-10-24 14:23:09', 'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).', 12356789, 0, 5, 2),
(8, 'ouvert', '2020-10-24 14:45:42', 'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.', 65438907, 0, 4, 1),
(9, 'ouvert', '2020-10-25 16:01:29', 'Test', 54678902, 0, 4, 1),
(10, 'ouvert', '2020-10-25 19:57:56', 'Je suis un ticket test 3831383833', 34474792, 0, 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`ID`, `login`, `password`, `email`, `isAdmin`) VALUES
(3, 'admin', 'd8014bd3051954f5b7a9b49adeb234b8a2865c27', 'a@a.a', 1),
(4, 'nicolas', '$2y$10$bgDoTwrufSoBncyclkr22ulHiIGtbc6e.yVP.NdGUARTiT3DG8qB.', 'b@b.b', 0),
(5, 'test', '$2y$10$f23N1I28MZRxEtwzSaYaqeSvpTyqMaAIkNO1LLxDo/OACgmWWyIrG', 'c@c.c', 0),
(6, 'admin2', 'd8014bd3051954f5b7a9b49adeb234b8a2865c27', 'd@d.d', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_ticket` (`ID_ticket`);

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_categorie` (`ID_categorie`),
  ADD KEY `ID_user` (`ID_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `reponse`
--
ALTER TABLE `reponse`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD CONSTRAINT `reponse_ibfk_1` FOREIGN KEY (`ID_ticket`) REFERENCES `ticket` (`ID`);

--
-- Contraintes pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`ID_categorie`) REFERENCES `categorie` (`ID`),
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`ID_user`) REFERENCES `user` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
