<?php
session_start();
require_once '../db.php';
require_once '../lib.php';

if ($_SESSION['login'] === 'admin') {

    //variables
    $sens = "";
    $order = "";
    $column = "";
    $isVisible;
    $hidden_id;
    $filter = null;
    $stats = [];
    $reponse = [];
    $stat_open = 0;
    $stat_in_progress = 0;
    $stat_close = 0;

    //assainir variables
    if (isset($_GET['sens'])) {
        $sens = assainir($_GET['sens']);
    } else {
        $sens = "ASC";
    }
    if (isset($_GET['order'])) {
        $order = assainir($_GET['order']);
    } else {
        $order = "date_ticket";
    }

    // requetes statistiques et injection resultat dans un tableau
    $stat = "SELECT ticket.statut, COUNT(reponse.ID) as count_reponse, ticket.date_ticket FROM ticket
    LEFT JOIN reponse on ticket.ID = reponse.ID_ticket
    JOIN categorie on ticket.ID_categorie = categorie.ID
    WHERE  ticket.date_ticket > DATE_SUB(CURDATE(), INTERVAL 15 DAY)
    GROUP BY ticket.ID";

    $stats_result = mysqli_query($db, $stat);
    if (mysqli_num_rows($stats_result)) {
        while ($row_result = mysqli_fetch_assoc($stats_result)) {
            $stats[] = $row_result;
        }
        for ($i = 0; $i < count($stats); $i++) {
            if ($stats[$i]['statut'] == 'resolu') {
                $stat_close++;
            }
            if ($stats[$i]['statut'] == 'en cours') {
                $stat_in_progress++;
            }
            if ($stats[$i]['statut'] == 'ouvert') {
                $stat_open++;
            }

        }
    }

    //pour filter resultats
    if (isset($_POST['filter'])) {
        $filter = assainir($_POST['filter']);
    }

    if ($filter == null || $filter == 'tous') {
        $requete = "SELECT ticket.ID as id_ticket, date_ticket, ticket.statut, categorie.nom, ticket.isVisible FROM ticket
    LEFT JOIN reponse on ticket.ID = reponse.ID_ticket
    JOIN categorie on ticket.ID_categorie = categorie.ID
    GROUP BY ticket.ID
    ORDER BY $order $sens";
    } else {
        $requete = "SELECT ticket.ID as id_ticket, date_ticket, ticket.statut, categorie.nom, ticket.isVisible FROM ticket
        LEFT JOIN reponse on ticket.ID = reponse.ID_ticket
        JOIN categorie on ticket.ID_categorie = categorie.ID
        WHERE ticket.statut = '$filter'
        GROUP BY ticket.ID
        ORDER BY $order $sens";
    }

    //mettre resultat dans tableau pour afficher dans table html
    $result = mysqli_query($db, $requete);
    if (mysqli_num_rows($result)) {
        while ($row_result = mysqli_fetch_assoc($result)) {
            $reponse[] = $row_result;
        }
    }

    //requete pour afficher un ticket resolu dans le public
    if (isset($_POST['submit'])) {
        $isVisible = 1;
        if (isset($_POST['hidden_id'])) {
            $hidden_id = assainir($_POST['hidden_id']);

        }
        $requete = "UPDATE ticket set isVisible = $isVisible WHERE ID = $hidden_id";
        mysqli_query($db, $requete);
        header('Location: dashboard.php');
    }

    //requete pour retirer un ticket resolu du public
    if (isset($_POST['submit_delete_public'])) {
        $isVisible = 0;

        if (isset($_POST['hidden_id'])) {
            $hidden_id = assainir($_POST['hidden_id']);

        }
        $requete = "UPDATE ticket set isVisible = $isVisible WHERE ID = $hidden_id";
        mysqli_query($db, $requete);
        header('Location: dashboard.php');
    }
    mysqli_close($db);

} else {
    header('Location: ../index.php');
    die;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/template_nav.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Dashboard</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="../index.php">Accueil</a></li>
                <li class="connect"><a href="../logout.php">Logout</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Bienvenue administrateur !! </h1>
        <h2>Etat des tickets</h2>
        <div>
            <form action="" id="filter" method="POST">
                <span>Filtrer par : </span> <select name="filter">
                    <option value="tous">tous les statuts</option>
                    <option value="ouvert">Ouvert</option>
                    <option value="en cours">En cours</option>
                    <option value="resolu">Résolu</option>
                </select>
                <input type="submit" name="submit_filter" value="FILTRER">
            </form>
            <table id="table_info">
                <thead>
                    <tr>

                        <th>ID ticket</th>
                        <th>
                            <?php
if ($order === "date_ticket") {
    if ($sens === "ASC") {?>
                            <a href="dashboard.php?order=date_ticket&sens=DESC">Date</a><i class="fas fa-sort-up"></i>
                            <?php } else {?>
                            <a href="dashboard.php?order=date_ticket&sens=ASC">Date <i class="fas fa-sort-down"></i></a>
                            <?php }} else {?>
                            <a href="dashboard.php?order=date_ticket&sens=ASC">Date</a><i
                                class="fas fa-sort-down"></i></a>
                            <?php }?>
                        </th>
                        <th>
                            <?php
if ($order === "nom") {
    if ($sens === "ASC") {?>
                            <a href="dashboard.php?order=nom&sens=DESC">Categories</a><i class="fas fa-sort-up"></i>
                            <?php } else {?>
                            <a href="dashboard.php?order=nom&sens=ASC">Categories <i class="fas fa-sort-down"></i></a>
                            <?php }} else {?>
                            <a href="dashboard.php?order=nom&sens=ASC">Categories</a><i
                                class="fas fa-sort-down"></i></a>
                            <?php }?>
                        <th>
                            <?php
if ($order === "statut") {
    if ($sens === "ASC") {?>
                            <a href="dashboard.php?order=statut&sens=DESC">Statut</a><i class="fas fa-sort-up"></i>
                            <?php } else {?>
                            <a href="dashboard.php?order=statut&sens=ASC">Statut <i class="fas fa-sort-down"></i></a>
                            <?php }} else {?>
                            <a href="dashboard.php?order=statut&sens=ASC">Statut</a><i class="fas fa-sort-down"></i></a>
                            <?php }?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
for ($i = 0; $i < count($reponse); $i++) {
    $date = formatDate($reponse[$i]["date_ticket"]);

    ?><tr>
                        <td><?=$reponse[$i]["id_ticket"]?></td>
                        <td><?=$date?></td>
                        <td><?=$reponse[$i]["nom"]?>
                        </td>
                        <td><?=$reponse[$i]["statut"]?></td>

                        <td></a>
                            <a href="../ticket.php?id=<?=$reponse[$i]["id_ticket"]?>">Accéder a ce ticket</a>
                        </td>
                        <?php
if ($reponse[$i]["statut"] == 'resolu' && $reponse[$i]["isVisible"] != true) {
        ?>
                        <td>
                            <form action="" method="POST">
                                <input type="submit" name="submit" value="Ajouter publiquement">
                                <input type="hidden" name="hidden_id" value="<?=$reponse[$i]["id_ticket"]?>">
                            </form>
                        </td>
                        <?php
}
    if ($reponse[$i]["statut"] == 'resolu' && $reponse[$i]["isVisible"] == true) {
        ?>
                        <td>
                            <form action="" method="POST">
                                <input type="submit" name="submit_delete_public" value="Enlever du public">
                                <input type="hidden" name="hidden_id" value="<?=$reponse[$i]["id_ticket"]?>">
                            </form>
                        </td>
                        <?php
}
    ?>
                        <?php
}
?>
                </tbody>
            </table>
        </div>
        <h2>Stats tickets pour les 15 derniers Jours : </h2>
        <table id="table_stats">
            <thead>
                <tr>
                    <th>Ouvert</th>
                    <th>En cours</th>
                    <th>Résolu</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?=$stat_open?></td>
                    <td><?=$stat_in_progress?></td>
                    <td><?=$stat_close?></td>
                </tr>
            </tbody>
        </table>
    </main>
</body>

</html>