<?php
// On démarre la session
session_start();

//On require la connection à la BDD
require_once "../db.php";

// On déclare nos variables
$password = "";
$login = "";

if (isset($_SESSION['login'])) {
    if ($_SESSION['login'] == 'admin') {
        $login = $_SESSION['login'];
    }
}
if ($login == 'admin') {
    header('Location: dashboard.php');
    die();
} else {
    if (!empty($_POST)) {

        if (isset($_POST["password"])) {
            $password = $_POST["password"];
        }
        if (isset($_POST["login"])) {
            $login = $_POST["login"];
        }

        $request = "SELECT * from user where login = '$login' and password = SHA1('$password')";
        $result = mysqli_query($db, $request);
        if (mysqli_num_rows($result)) {
            $row = mysqli_fetch_assoc($result);
            $_SESSION['login'] = $login;
            header('Location: dashboard.php');
            die();
        } else {
            echo 'Tu cherches a faire quoi ici ???';
        }
    }
}

// On clos la connection à la BDD
mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/template_nav.css">
    <link rel="stylesheet" href="../css/connect_register.css">
    <title>Admin Login</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
            </ul>
        </nav>
    </header>
    <div id="form-box">
        <h1>Administrateur panel</h1>
        <form action="" method="post">
            <input type="text" class="conn_reg_form" name="login" placeholder="Login" required>
            <input type="password" class="conn_reg_form" name="password" placeholder="Mot de passe" required>
            <input type="submit" class="submit" value="se connecter">
        </form>
    </div>
</body>

</html>