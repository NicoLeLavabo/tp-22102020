<?php
session_start();
require 'db.php';

$error = array();
$login_change = false;
$login = $_SESSION['login'];

if (isset($_POST['login_submit'])) {
    if (isset($_POST['login'])) {
        $login = strip_tags($_POST['login']);
    }
    $login_request = "SELECT * FROM user WHERE login = '$login' LIMIT 1";
    $result = mysqli_query($db, $login_request);
    if (mysqli_num_rows($result)) {
        $error[] = 'Oups pseudo deja utilisé';
    } else {
        $older_login = $_SESSION['login'];
        $request = "UPDATE user SET login = '$login' WHERE login = '$older_login'";
        $result = mysqli_query($db, $request);
        $login_change = true;
        $_SESSION['login'] = $login;

    }
}

if (isset($_POST['password_submit'])) {
    if (isset($_POST['older_password'])) {
        $older_paswword = strip_tags($_POST['older_password']);
    }
    if (isset($_POST['password'])) {
        $password = strip_tags($_POST['password']);
    }
    if (isset($_POST['password_verify'])) {
        $password_verify = strip_tags($_POST['password_verify']);
    }
    if (empty($older_paswword)) {
        $error[] = "L'ancien mot de passe doit être renseigné";
    }
    if (strlen($_POST["password"]) <= 8) {
        $error[] = "Le mot de passe doit contenir 8 caractères minimum!";
    }
    if (!preg_match("#[0-9]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 chiffre minimum!";
    }
    if (!preg_match("#[A-Z]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 lettre capital minimum!";
    }
    if (!preg_match("#[a-z]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 lettre minuscule minimum!";
    }
    if (!preg_match("/\W/", $password)) {
        $error[] = "Le mot de passe doit contenir 1 caractère spécial minimum!";
    }

    if (!empty($older_paswword)) {
        $request = "SELECT * from user where login = '$login'";
        $result = mysqli_query($db, $request);
        $row = mysqli_fetch_assoc($result);
        if (password_verify($older_paswword, $row['password'])) {
            if ($password == $password_verify) {
                $password = password_hash($password, PASSWORD_DEFAULT);
                $requete = "UPDATE user SET password = '$password' WHERE login = '$login'";
                mysqli_query($db, $requete);
                mysqli_close($db);
                session_destroy();
                header("Location: connection.php");
                die();
            } else {
                $error[] = 'Attention les nouveaux mots de passe doivent être identiques';
            }
        } else {
            $error[] = 'Ancien mot de passe incorrect';
        }
    }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/profil.css">
    <title>Profil</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="user_tickets.php">Vos tickets</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <div id="form-box">
            <h1> Modifier profil</h1>
            <div class="error">
                <?php
for ($i = 0; $i < count($error); $i++) {
    ?>
                <p><?=$error[$i]?></p>
                <?php

}
?>
            </div>
            <form action="profil.php" method="POST">
                <input type="text" class="profil_form" name="login" value="<?=$_SESSION['login']?>" required>
                <input type="submit" class="submit" name="login_submit" value="modifier mon pseudo">
            </form>
            <div>
                <div id="separator"></div>
            </div>
            <form action="profil.php" method="POST">
                <input type="password" class="profil_form" name="older_password" placeholder="ancien mot de passe">
                <input type="password" class="profil_form" name="password" placeholder="nouveau mot de passe">
                <input type="password" class="profil_form" name="password_verify"
                    placeholder="mot de passe confirmation">
                <input type="submit" class="submit" name="password_submit" value="modifier mon mot de passe">
            </form>
        </div>
    </main>
</body>

</html>