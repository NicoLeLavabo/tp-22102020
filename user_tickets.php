<?php
session_start();
require_once "db.php";
require_once "lib.php";

//variables
$tickets = [];

//session
if ($_SESSION['login']) {
    if (isset($_SESSION['login'])) {
        $login = assainir($_SESSION['login']);
    }
    $requete = "SELECT ticket.statut, ticket.date_ticket, ticket.numAlea, categorie.nom FROM ticket
    INNER JOIN user on ticket.ID_user = user.ID
    INNER JOIN categorie on ticket.ID_categorie = categorie.ID
    WHERE user.login = '$login'
    ORDER BY ticket.date_ticket DESC";

    $result = mysqli_query($db, $requete);
    if (mysqli_num_rows($result)) {
        while ($row_result = mysqli_fetch_assoc($result)) {
            $tickets[] = $row_result;
        }

        mysqli_close($db);
    }
} else {
    //redirection
    header('Location : index.php');
    mysqli_close($db);
    die;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/user_tickets.css">
    <title>Vos tickets</title>
</head>
<header>
    <nav>
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="profil.php">Profil</a></li>
        </ul>
    </nav>
</header>

<body>
    <h1>Vos tickets <?=$login?></h1>
    <table>
        <thead>
            <tr>
                <th>Numéro du ticket</th>
                <th>Date du ticket</th>
                <th>Catégorie du tickets</th>
                <th>Statut du ticket</th>
            </tr>
        </thead>
        <tbody>
            <?php
for ($i = 0; $i < count($tickets); $i++) {
    $date = formatDate($tickets[$i]['date_ticket']);
    ?>
            <tr>
                <td> <?=$tickets[$i]['numAlea']?> </td>
                <td> <?=$date?></td>
                <td> <?=$tickets[$i]['nom']?> </td>
                <td> <?=$tickets[$i]['statut']?> </td>
            </tr>
            <?php
}
?>
        </tbody>
    </table>
</body>

</html>