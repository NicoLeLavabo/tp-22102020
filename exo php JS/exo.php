<?php

//Variables
$maVar;

//constante
define("Nombre", 10);

//structure de controle
if ($condition) {
    //action a faire
} else {
    //action a faire
}

//type de variable

$maVar = 1;

$maVar = false;

$maVar = "je suis une variable";

//fonction et parametre
function test_function($nombre)
{
    return $nombre + $nombre;
}

//procedure
function procedure()
{
    $nombre1 = 1;
    $nombre2 = 2;
    $result = 0;

    $result = $nombre1 + $nombre2;

    echo "resultat : $result";
}

//concatenation
$prenom = "nicolas";
$concat = 'Bonjour je suis ' . $prenom;
$concat2 = "Bonjour je suis $prenom";