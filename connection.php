<?php
session_start();
require_once "db.php";

$error = [];
if (!empty($_POST)) {
    $login = "";
    $password = "";

    if (isset($_POST['login'])) {
        $login = strip_tags($_POST['login']);
    }
    if (isset($_POST['password'])) {
        $password = strip_tags($_POST['password']);
    }

    if (!empty($login) && !empty($password)) {
        $password_request = "SELECT password from user where login = '$login'";
        $result = mysqli_query($db, $password_request);
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row['password'])) {
            $_SESSION['login'] = $login;
            mysqli_close($db);
            header("Location: index.php");
            die();
        } else {
            $error[] = 'Login ou mot de passe incorrect';
        }
    }
}
if (!empty($error)) {
    mysqli_close($db);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connection</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/connect_register.css">
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="inscription.php">Inscription</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <div id="form-box">
            <h1>Vous connecter</h1>
            <div class="error">
                <?php
for ($i = 0; $i < count($error); $i++) {
    ?>
                <p><?=$error[$i]?></p>
                <?php

}
?>
            </div>
            <form action="connection.php" method="POST">
                <input class="conn_reg_form" type="text" name="login" placeholder="Login" required>
                <input class="conn_reg_form" type="password" name="password" placeholder="Mot de passe">
                <input class="submit" type="submit" name="connexion_submit" value="Connexion">
            </form>
        </div>
    </main>

</body>

</html>