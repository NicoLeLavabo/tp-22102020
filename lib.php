<?php
function formatDate($date)
{
    $date = date_create($date);
    $date = date_format($date, 'd/m/y à H:i:s');
    return $date;
}

function assainir($var)
{
    $var = addslashes(strip_tags($var));
    return $var;
}