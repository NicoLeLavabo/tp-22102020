<?php
// On démarre une session
session_start();

// On require la connection à la BDD
require_once "db.php";
require_once "lib.php";

//Déclaration des variables
$categorie = "";
$email = "";
$message = "";
$login = "";
$numAlea = rand(10000000, 99999999);

if (isset($_SESSION['login'])) {
    $login = assainir($_SESSION['login']);
}
// Si on a des variables $_POST
if (!empty($_POST)) {
    if (isset($_POST['categorie'])) {
        $categorie = assainir($_POST['categorie']);
    }
    if (isset($_POST['message'])) {
        $message = assainir($_POST['message']);
    }

    $id_request = "SELECT ID FROM user WHERE login = '$login'";
    echo $id_request;
    $id_result = mysqli_query($db, $id_request);
    if (mysqli_num_rows($id_result)) {
        $row = mysqli_fetch_assoc($id_result);
        $id = $row['ID'];
    }

/* CREATION D UN NOUVEAU TICKET */
// On créer la requete d'insertion
    $new_ticket = "INSERT INTO ticket (message_ticket, ID_categorie, numAlea, ID_user) VALUES ('$message', '$categorie', $numAlea, $id)";
    mysqli_query($db, $new_ticket);
    header('Location: index.php');
}

// On recupère les catégories
$requete = "SELECT nom, ID as ID_categorie FROM categorie";
$result = mysqli_query($db, $requete);

//Si on a un résultat
if (mysqli_num_rows($result)) {
    while ($row_result = mysqli_fetch_assoc($result)) {
        // Pour chaque categorie on ajoute le nom dans le tableau
        $categories[] = $row_result;
    }
}

// On clos la connection
mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/new_ticket.css">
    <title>Nouveau ticket</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
            </ul>
        </nav>
    </header>
    <div id="form-box">
        <h1>Nouveau ticket</h1>
        <form action="new_ticket.php" method="POST">
            <select class="select_ticket" name="categorie" id="" required>
                <option value="">--- choisissez une catégorie</option>
                <?php
for ($i = 0; $i < count($categories); $i++) {
    ?>
                <option name="<?=$categories[$i]["ID_categorie"]?>" value="<?=$categories[$i]["ID_categorie"]?>">
                    <?=$categories[$i]["nom"]?>
                </option>
                <?php
}
?>
            </select>
            <textarea class="select_ticket" type="text" name="message" row="50" cols="30"
                placeholder="Votre message ..." required></textarea>
            <input class="submit" type="submit" value="Envoyer un nouveau ticket">
        </form>
</body>

</html>