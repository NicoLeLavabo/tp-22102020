<?php
session_start();
require_once "db.php";
require_once "lib.php";
$login = "";
$email = "";
$error = array();

if (!empty($_POST)) {

    if (isset($_POST['login'])) {
        $login = assainir($_POST['login']);
    }
    if (isset($_POST['password'])) {
        $password = assainir($_POST['password']);}
    if (isset($_POST['password_verify'])) {
        $password_verify = assainir($_POST['password_verify']);}
    if (isset($_POST['email'])) {
        $email = assainir($_POST['email']);
    }

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error[] = "Email non valide!";
    }
    if (strlen($_POST["password"]) <= 8) {
        $error[] = "Le mot de passe doit contenir 8 caractères minimum!";
    }
    if (!preg_match("#[0-9]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 chiffre minimum!";
    }
    if (!preg_match("#[A-Z]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 lettre capital minimum!";
    }
    if (!preg_match("#[a-z]+#", $password)) {
        $error[] = "Le mot de passe doit contenir 1 lettre minuscule minimum!";
    }
    if (!preg_match("/\W/", $password)) {
        $error[] = "Le mot de passe doit contenir 1 caractère spécial minimum!";
    }

    if (!empty($error)) {
        mysqli_close($db);
    }

    if ($password == $password_verify) {
        $email_request = "SELECT * FROM user WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($db, $email_request);
        echo $email_request;
        if (mysqli_num_rows($result)) {
            $error[] = "Email saisi non disponible!";
        } else {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $requete = "INSERT INTO user (login, password, email, isAdmin) VALUES ('$login', '$password', '$email', FALSE)";
            echo $requete;
            mysqli_query($db, $requete);
            mysqli_close($db);
            header("Location: connection.php");
            die();
        }
    } else {
        $error[] = 'Les mots de passe doivent être identiques';
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/connect_register.css">

    <title>Inscription</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="connection.php">connection</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <div id="form-box">
            <h1>Vous inscrire</h1>
            <div class="error">
                <?php
for ($i = 0; $i < count($error); $i++) {
    ?>
                <p><?=$error[$i]?></p>
                <?php

}
?>
            </div>
            <form action="inscription.php" method="POST">
                <input class="conn_reg_form" type="text" name="login" placeholder="Login" value="<?=$login?>" required>
                <input class="conn_reg_form" type="password" name="password" placeholder="Mot de passe" required>
                <input class="conn_reg_form" type="password" name="password_verify"
                    placeholder="Mot de passe confirmation" required>
                <input class="conn_reg_form" type="email" name="email" placeholder="Email" value="<?=$email?>" required>
                <input class="submit" type="submit" name="connexion_submit" value="inscription">
            </form>
        </div>
    </main>
</body>

</html>