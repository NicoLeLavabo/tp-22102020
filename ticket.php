<?php
// On démarre une session
session_start();

// On require la connection à la BDD
require_once "db.php";
require_once "lib.php";

// declaration des variables
$id = "";
$reponse = [];
$admin = 0;
$statut = "";
$isVisible;

if (isset($_GET['id'])) {
    $id = assainir($_GET['id']);
}
if (isset($_SESSION["login"])) {
    if ($_SESSION['login'] == 'admin') {
        $admin = 1;
    }
}
//On recupère toutes les reponses du message
$requete = "SELECT * FROM ticket
LEFT JOIN reponse on ticket.ID = reponse.ID_ticket
INNER JOIN user on ticket.ID_user = user.ID
WHERE ticket.id = $id
ORDER BY reponse.message_date DESC";

$result = mysqli_query($db, $requete);
if (mysqli_num_rows($result)) {
    while ($row_result = mysqli_fetch_assoc($result)) {
        $reponse[] = $row_result;
    }

    for ($i = 0; $i < count($reponse); $i++) {
        if ($reponse[$i]['message_admin'] == 1 && $reponse[$i]['statut'] == 'ouvert') {
            $requete_update_statut = "UPDATE ticket SET ticket.statut = 'en cours' WHERE ticket.ID = '$id'";
            mysqli_query($db, $requete_update_statut);
            header('Location: ticket.php?id=' . $id);
        }
    }
}

if (!empty($_POST)) {

    if (isset($_POST['submit_response'])) {
        if (isset($_POST['reponse_message'])) {
            $reponse_message = assainir($_POST["reponse_message"]);
        }

        // On insère une nouvelle réponse
        $new_reponse = "INSERT INTO reponse (ID_ticket, message_reponse, message_admin) VALUES ($id, '$reponse_message', $admin)";
        echo $new_reponse;
        mysqli_query($db, $new_reponse);
        header('Location: ticket.php?id=' . $id);
    }
    if (isset($_POST['submit_statut'])) {
        if (isset($_POST['statut'])) {
            $statut = assainir($_POST["statut"]);
        }
        $isVisible = $reponse[0]['isvisible'];
        if ($isVisible == 1) {
            $isVisible = 0;
        }
//Changer statut pour le ticket quand admin
        $requete = "UPDATE ticket SET statut = '$statut', isvisible = $isVisible WHERE ID = $id";
        echo $requete;
        $result = mysqli_query($db, $requete);
        header('Location: ticket.php?id=' . $id);
    }
}

// On clos la connection
mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/ticket.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Ticket</title>
</head>

<body>

    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <?php
if (isset($_SESSION['login'])) {
    if ($_SESSION['login'] == 'admin') {
        ?>
                    <li><a href="admin/dashboard.php">Dashboard</a></li>
                    <li class="connect"><a href="logout.php">Logout</a></li>
                    <?php
} else if ($_SESSION['login'] != 'admin') {
        ?>
                    <li><a href="new_ticket.php">Créer un nouveau ticket</a></li>
                    <li class="connect"><a href="logout.php">Logout</a></li>
                    <?php }} else {?>
                    <li class="connect"><a href="inscription.php">Inscription</a></li>
                    <li><a href="connection.php">Connection</a></li>
                    <?php
}
?>
                </ul>
            </nav>
        </header>
        <main>
            <h1>Réponse au sujet</h1>
            <div id="main_ticket">
                <div id="info">
                    <p><?=$reponse[0]["login"]?></p>
                    <div id="statut">
                        <p><?=$reponse[0]["statut"]?></p>
                        <?php
if ($reponse[0]["statut"] == 'resolu') {
    ?>
                        <i style="color:red" class="fas fa-circle"></i>
                        <?php
} else if ($reponse[0]["statut"] == 'en cours') {
    ?>
                        <i style="color:orange" class="fas fa-circle"></i>
                        <?php
} else if ($reponse[0]["statut"] == 'ouvert') {
    ?>
                        <i style="color:green" class="fas fa-circle"></i>
                        <?php
}
?>

                    </div>
                    <?php
if (isset($_SESSION['login'])) {
    if ($_SESSION['login'] == 'admin') {
        ?>
                    <form action="" method="POST">
                        <select name="statut" id="" required>
                            <option value="">--- choisissez un statut</option>
                            <option value="ouvert">ouvert</option>
                            <option value="en cours">en cours</option>
                            <option value="resolu">resolu</option>
                        </select>
                        <input type="submit" name="submit_statut" value="Modifier le statut">
                    </form>
                    <?php
}}
?>

                    <p>Le <?=formatDate($reponse[0]["date_ticket"])?></p>
                </div>
                <p><?=$reponse[0]["message_ticket"]?></p>
            </div>
            <?php
if (!empty($reponse[0]['message_date'])) {
    for ($i = 0; $i < count($reponse); $i++) {
        if ($reponse[$i]['message_admin'] == 1) {
            ?>
            <div id="isAdmin">
                <p>
                    Admin
                </p>
                <p>
                    <?=$reponse[$i]["message_reponse"]?>
                </p>
            </div>

            <?php
} else {
            ?>
            <div id="noAdmin">
                <p>
                    <?=$reponse[$i]['login']?>
                </p>
                <p>
                    <?=$reponse[$i]["message_reponse"]?>
                </p>
            </div><?php
}}
} else if (!empty($reponse[0]['message_date']) && $reponse[0]['statut'] != 'resolu') {
    echo '<h3> Aucune réponse actuellement</h3>';
}

if ($reponse[0]['statut'] != 'resolu') {
    ?>
            <form action="" method="post">
                <textarea name="reponse_message" cols="70" rows="10"></textarea>
                <input type="submit" name="submit_response" value="Répondre">
            </form>
            <?php
}?>
        </main>
    </body>
</body>

</html>