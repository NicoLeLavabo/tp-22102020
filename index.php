<?php
// On démarre la session
session_start();

//On require la connection à la BDD
require_once "db.php";
require_once "lib.php";

// On déclare les variables
$ticket = [];
$ticket_public = [];

$requete = "SELECT ticket.ID as id_ticket, date_ticket, ticket.isVisible, message_ticket, login FROM ticket
    INNER JOIN user ON ticket.ID_user = user.ID
    WHERE ticket.isVisible = TRUE ";
$result = mysqli_query($db, $requete);
// On verifie que la valeur du $user_ticket existe dans la base de données
if (mysqli_num_rows($result)) {
    while ($result_row = mysqli_fetch_assoc($result)) {
        $ticket_public[] = $result_row;
    }
}
// On verifie si on a des variables en $_POST
if (!empty($_POST)) {
    if (isset($_POST['user_ticket'])) {
        $user_ticket = assainir($_POST['user_ticket']);
    }
    // On crée une requete pour verifier la valeur de $user_ticket
    $requete = "SELECT ticket.ID as id_ticket, ticket.numAlea, date_ticket, user.email, user.login, ticket.isVisible FROM ticket
    INNER JOIN user ON ticket.ID_user = user.ID
    WHERE ticket.numAlea = '$user_ticket' OR user.email = '$user_ticket'
    ORDER BY date_ticket DESC";
    $result = mysqli_query($db, $requete);
    // On verifie que la valeur du $user_ticket existe dans la base de données
    if (mysqli_num_rows($result)) {
        while ($result_row = mysqli_fetch_assoc($result)) {
            if ($user_ticket === $result_row["numAlea"]) {
                $ticket[] = $result_row;
            }
            if ($user_ticket === $result_row["email"]) {
                $ticket[] = $result_row;
            }
        }
    }
}

// On clos la connection à la BDD
mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/template_nav.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Document</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <?php
if (isset($_SESSION['login']) && $_SESSION['login'] == 'admin') {
    ?>
                <li><a href="admin/dashboard.php">Dashboard</a></li>
                <li class="connect"><a href="logout.php">Logout</a></li>
                <?php
}
if (isset($_SESSION['login']) && $_SESSION['login'] != 'admin') {
    ?>
                <li><a href="new_ticket.php">Créer un nouveau ticket</a></li>
                <li id="form">
                    <form method="POST">
                        <input class="search" type="text" name="user_ticket" placeholder="Numero de ticket ou email...">
                        <button class="submit" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </li>
                <li><a href="profil.php">Profil</a></li>
                <li><a href="logout.php">Logout</a></li>
                <?php
} else if (!isset($_SESSION['login'])) {
    ?>

                <li class="connect"><a href="inscription.php">Inscription</a></li>
                <li><a href="connection.php">Connection</a></li>

                <?php
}
?>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Bienvenue sur le support de ticket</h1>
        <?php
if (count($ticket) > 0) {
    ?>
        <h2>Vos tickets</h2>
        <ul>
            <?php

    for ($i = 0; $i < count($ticket); $i++) {
        ?>
            <li><i class="fas fa-ticket-alt"></i><a href="ticket.php?id=<?=$ticket[$i]["id_ticket"]?>">Consulter votre
                    ticket du :
                    <?=formatDate($ticket[$i]["date_ticket"])?></a>
            </li>
            <?php
}}
?>

        </ul>
        <h2>Les problèmes résolus</h2>
        <div id="main_faq">


            <?php
for ($i = 0; $i < count($ticket_public); $i++) {
    if ($ticket_public[$i]['isVisible'] == true) {
        $date = formatDate($ticket_public[$i]["date_ticket"]);
        ?>

            <div id="secondary_faq">
                <div id="title">
                    <h3><?=$ticket_public[$i]['login']?></h3>
                    <p>Le <?=$date?></p>
                </div>
                <p><?=$ticket_public[$i]['message_ticket']?></p>
                <p></p>
                <a href="ticket.php?id=<?=$ticket_public[$i]["id_ticket"]?>">Voir ce ticket</a>
            </div>
            <?php
}
    ?>

            <?php
}
?>

        </div>
    </main>

</body>

</html>